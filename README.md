Magento 2 Mandaê Shipping Method

**Manual**

Caso não exista, crie uma pasta "app/code/"  
Coloca o módulo dentro dessa pasta  

Pela linha de comando digite  
`php bin/magento setup:upgrade`

**Composer**

- `composer config repositories.mandae-magento2 git https://bitbucket.com/biz-thirdparty/mandae-magento2.git`
- `composer require biz-thirdparty/mandae-magento2:dev-master`
- `php bin/magento setup:upgrade`
- `php bin/magento setup:di:compile`
- `php bin/magento cache:flush`