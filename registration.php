<?php
/**
 * @package     Mandae_Shipping
 * @author      Mandaê
 * @copyright   Mandaê - https://www.mandae.com.br
 * @license     https://opensource.org/licenses/AFL-3.0  Academic Free License 3.0 | Open Source Initiative
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Mandae_Shipping',
    __DIR__
);