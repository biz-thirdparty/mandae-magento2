<?php
/**
 * @package     Mandae_Shipping
 * @author      Mandaê
 * @copyright   Mandaê - https://www.mandae.com.br
 * @license     https://opensource.org/licenses/AFL-3.0  Academic Free License 3.0 | Open Source Initiative
 */

namespace Mandae\Shipping\Setup;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Type;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    /**
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     */
    public function __construct(\Magento\Eav\Setup\EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory */
        $eavSetupFactory = $this->eavSetupFactory->create(['setup' => $setup]);

        $eavSetupFactory->addAttribute(
            Product::ENTITY,
            'width',
            $this->getAttributeData('Width', 71)
        );

        $eavSetupFactory->addAttribute(
            Product::ENTITY,
            'height',
            $this->getAttributeData('Height', 72)
        );

        $eavSetupFactory->addAttribute(
            Product::ENTITY,
            'length',
            $this->getAttributeData('Length', 73)
        );
    }

    /**
     * Get simple attribute data.
     */
    protected function getAttributeData($label, $sortOrder)
    {
        $productTypes = join(',', [Type::TYPE_SIMPLE, Type::TYPE_VIRTUAL]);

        $attributeData = [
            'type' => 'text',
            'label' => __($label),
            'input' => 'text',
            'sort_order' => $sortOrder,
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'user_defined' => true,
            'required' => false,
            'used_in_product_listing' => true,
            'apply_to' => $productTypes,
            'group' => 'General',
            'unique' => false,
            'visible_on_front' => false,
            'searchable' => false,
            'filterable' => false,
            'comparable' => false,
            'visible' => true,
            'class' => 'validate-number validate-zero-or-greater',
        ];

        return $attributeData;
    }
}