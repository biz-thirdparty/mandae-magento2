<?php
/**
 * Mandaê
 *
 * @category   Mandae
 * @package    Mandae_Shipping
 * @author     Thiago Contardi
 * @copyright  Mandaê - https://www.mandae.com.br
 * @license    https://opensource.org/licenses/AFL-3.0  Academic Free License 3.0 | Open Source Initiative
 */

namespace Mandae\Shipping\Helper;

use \Magento\Framework\Filter\RemoveAccents;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var string
     */
    protected $_storeScope;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->_storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $this->_scopeConfig = $scopeConfig;

        return parent::__construct($context);
    }

    /**
     * @param $message
     * @return void
     */
    public function log($message)
    {
        if (($this->_scopeConfig->getValue('carriers/mandae/debug', $this->_storeScope))) {
            $this->_logger->debug($message);
        }
    }

    /**
     * @return bool
     */
    public function isSandbox()
    {
        if ($this->_scopeConfig->getValue('carriers/mandae/environment', $this->_storeScope) == 'sandbox') {
            return true;
        }

        return false;
    }

    /**
     * Remove Latin characters ans special chars from string
     *
     * @param string $str
     * @return string
     */
    public function slugify($str)
    {
        $filter = new RemoveAccents();
        $str = $filter->filter($str);

        $urlKey = preg_replace('#[^0-9a-z+]+#i', '-', $str);
        $urlKey = strtolower($urlKey);
        $urlKey = trim($urlKey, '-');

        return $urlKey;
    }

}