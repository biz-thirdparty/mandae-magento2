<?php
/**
 * @package     Mandae_Shipping
 * @author      Mandaê
 * @copyright   Mandaê - https://www.mandae.com.br
 * @license     https://opensource.org/licenses/AFL-3.0  Academic Free License 3.0 | Open Source Initiative
 */

namespace Mandae\Shipping\Model\Source;

class Methods implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'expresso', 'label' => __('Express')],
            ['value' => 'economico', 'label' => __('Economy')],
            ['value' => 'rapido', 'label' => __('Fast')]
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'expresso' => __('Express'),
            'economico' => __('Economy'),
            'rapido' => __('Fast')
        ];
    }
}
