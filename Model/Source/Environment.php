<?php
/**
 * @package     Mandae_Shipping
 * @author      Mandaê
 * @copyright   Mandaê - https://www.mandae.com.br
 * @license     https://opensource.org/licenses/AFL-3.0  Academic Free License 3.0 | Open Source Initiative
 */

namespace Mandae\Shipping\Model\Source;

class Environment implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'sandbox', 'label' => __('Sandbox')],
            ['value' => 'live', 'label' => __('Live')]
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return ['sandbox' => __('Sandbox'), 'live' => __('Live')];
    }
}
