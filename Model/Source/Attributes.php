<?php
/**
 * @package     Mandae_Shipping
 * @author      Mandaê
 * @copyright   Mandaê - https://www.mandae.com.br
 * @license     https://opensource.org/licenses/AFL-3.0  Academic Free License 3.0 | Open Source Initiative
 */

namespace Mandae\Shipping\Model\Source;

class Attributes implements \Magento\Framework\Option\ArrayInterface
{
    protected $objectManager;

    public function __construct(
        \Magento\Framework\ObjectManagerInterface $interface
    )
    {
        // $this->customerFactory = $customerFactory;
        $this->objectManager = $interface;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $attributes = $this->objectManager->get('Magento\Catalog\Model\Product')->getAttributes();

        $result = ['value' => '', 'label' => __('Please Select...')];
        foreach ($attributes as $attribute) {
            array_push($result, [
                'value' => $attribute->getAttributeCode(),
                'label' => $attribute->getFrontend()->getLabel()
                    ? $attribute->getFrontend()->getLabel() . ' (' . $attribute->getAttributeCode() . ')'
                    : $attribute->getAttributeCode()
            ]);
        }

        return $result;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $attributes = $this->objectManager->get('Magento\Catalog\Model\Product')->getAttributes();

        $result = ['' => __('Please Select...')];
        foreach ($attributes as $attribute) {
            $result[$attribute->getAttributeCode()] = $attribute->getFrontend()->getLabel() ?: $attribute->getAttributeCode();
        }

        return $result;
    }
}
